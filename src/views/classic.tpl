<!doctype html>
<html>
%include('head.tpl')
<body>
    <header>
        <h1>{{title}}</h1>
        <nav>
            %include('backtomain.tpl')
        </nav>
    </header>

    <main> {{ !htmlstuff }} </main>

    % include('footer.tpl')
    % include('scripts.tpl')
</body>
</html>

