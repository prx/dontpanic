<script>
    var month = new Date().getMonth();

    if (month >= 0 && month < 3) {
            document.body.className = "winter";
    }
    else if (month >= 3 && month < 6) {
            document.body.className = "spring";
    }
    else if (month >= 6 && month < 9) {
            document.body.className = "summer";
    }
    else if (month >= 9 && month < 11) {
            document.body.className = "autumn";
    }
    else {
            document.body.className = "december";
    }
</script>
