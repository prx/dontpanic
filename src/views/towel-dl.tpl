<!doctype html>
<html>
%include('head.tpl')
<body>
    <header>
        <h1>{{movie_name}}</h1>
        <nav>
            %include('backtomain.tpl')
        </nav>
    </header>

    <main class="middle-center">
        <span id="status">{{ !status }}</span>
        <div class="big">
            <p><span id="playlink">{{ !playlink }}</span></p>
        </div>
        <br />
        <div>
            <span id="subtitles">{{ !subtitles }}</span>
            <span id="stop">{{ !stop }}</span>
        </div>
    </main>

    % include('footer.tpl')
    % include('scripts.tpl')

    <script>
        function update_values() {
            var request = new XMLHttpRequest();
            request.open('GET', '/_getinfos/{{movie_name}}/{{index}}', true);

            request.onload = function() {
                if (request.status >= 200 && request.status < 400) {
                    data = JSON.parse(request.responseText);
					document.getElementById("status").innerHTML = data.status;
					document.getElementById("playlink").innerHTML = data.playlink;
					document.getElementById("subtitles").innerHTML = data.subtitles;
					document.getElementById("stop").innerHTML = data.stop;
                }
            }
            request.send();
        }
        setInterval("update_values();", 1000);
    </script>
</body>
</html>


