<!doctype html>
<html>
%include('head.tpl')
<body>
    <header>
        <h1>{{tpl_configuration}}</h1>
        <nav>
            %include('backtomain.tpl')
        </nav>
    </header>

    <main class="text-left">
        <em>{{tpl_config_restart}}</em>

        <ul style="list-style-type: square;">
            
            <form action="/_applyconfig" method="post">
                <h3 style="margin-top:20px">{{tpl_general}}</h3>
                <li> {{tpl_favourite_search_engine}} : 
                    {{!search_engines}}
                </li>
                <li> {{tpl_show_play_link}}
                    <input name="startperc" type="text" style="width:2em;" value="{{startperc}}"> %
                </li>
                <li> {{tpl_upload_limit}}
                    <input name="uplimit" type="text" value="{{uplimit}}">
                </li>
                <li> {{tpl_download_limit}}
                    <input name="downlimit" type="text" value="{{downlimit}}">
                </li>

                <h3 style="margin-top:20px">Proxy</h3>
                <p>{{ !tpl_proxy_explanations }}</p>
                <li> 
                    <a href⁼"#" title="{{tpl_proxy_type_exp}}"> {{tpl_proxy_type}} : 
                    <select name="proxy_type" size="1">
                        <option> {{proxy_type}}
                        <option> none
                        <option> socks5
                        <option> socks4
                        <option> socks5_pw
                        <option> http
                        <option> http_pw
                        <option> i2p_proxy
                    </select>
                    </a>
                </li>
                <li> {{tpl_proxy_hostname}} :
                    <input name="proxy_hostname" type="text" value="{{proxy_hostname}}" placeholder="112.65.135.54:8080">
                </li>
                <li> {{tpl_proxy_username}} : 
                    <input name="proxy_username" type="text" value="{{proxy_username}}">
                </li>
                <li> {{tpl_proxy_password}} : 
                    <input name="proxy_password" type="text" value="{{proxy_password}}">
                </li>
                <input value="{{tpl_apply_changes}}" type="submit" style="margin-top:10px">
            </form>

            <h3 style="margin-top:20px">{{tpl_storage}}</h3>
            <li> {{cache}} {{tpl_incache}}:
                <a href="/clean">{{tpl_clean}}</a>
            </li>
            <li> {{ntor}} torrent(s):
                <a href="/cleantorrent">{{tpl_remove_all_t}}</a>
            </li>
        </ul>
    </main>

    % include('footer.tpl')
     % include('scripts.tpl')
</body>
</html>


