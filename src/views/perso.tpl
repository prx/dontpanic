<!doctype html>
<html>
%include('head.tpl')
<body>
    <header>
        <h1>{{tpl_yourpage}} {{username}}</h1>

        <nav>
            %include('backtomain.tpl')

            <div class="float-right">
                <a href="/_admin">{{tpl_admin}}</a> -
                <a href="/logout">{{tpl_logout}}</a> -
                <a href="/unsubscribe">{{tpl_unsubscribe}}</a>
            </div>
        </nav>
    </header>

    <main class="text-left">
        <h2>{{tpl_watchlist}}</h2>
        <hr>
        %if len(movies) == 0 and len(series) == 0:
            <p class="text-center">{{tpl_watch_nothing}}</p>
        %else:
            %if len(movies) > 0:
            <h3>{{tpl_movies}}</h3>
            <div>
                {{ !movies }}
            </div>
            %end

            %if len(series) > 0:
            <h3>{{tpl_series}}</h3>
            <div>
                {{ !series }}
            </div>
            %end
        %end
    </main>

    % include('footer.tpl')
    % include('scripts.tpl')
</body>
</html>
