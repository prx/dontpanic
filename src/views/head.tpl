<head>
    <title>Don't panic!</title>

    <meta charset="UTF-8">

    <link rel="shortcut icon" sizes="32x32" href="/static/favicon.png">
    <link rel="apple-touch-icon" sizes="200x200" href="/static/favicon.png">

    <link rel="stylesheet" type="text/css" href="/static/style.css">
    <link rel="stylesheet" type="text/css" href="/static/grid.css">
    <link rel="stylesheet" type="text/css" href="/static/tooltip.css">
    <link rel='stylesheet' type="text/css" href='/static/perfect-scrollbar.min.css' />
</head>
