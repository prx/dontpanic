import gettext
import locale
import logging
import os
import sys

# Global directories
currentdir = os.path.dirname(os.curdir)
# Translation directories
localedir = os.path.join(currentdir, 'locale')
# Util global variables
progname = "dontpanic"

logger = logging.getLogger(__name__)

gettext.bindtextdomain(progname, localedir)
i18n_domain = gettext.textdomain(progname)

current_locale, encoding = locale.getdefaultlocale()
language = gettext.translation(progname, localedir, languages=[current_locale], fallback=True)
language.install()

# FIXME: The logging at the moment is not working here, because we use the logger before its init
# A translated language has been found
if language.info():
    logger.info("Set {} language to {}".format(progname, current_locale))
# No translated language has been found
else:
    logger.info("Your language ({}) is not translated".format(current_locale))

_ = language.gettext
